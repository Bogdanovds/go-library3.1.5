package services

type LibraryServices interface {
	GetUsersWithBooks() ([]byte, error)
	CreateAuthor([]byte) error
	GetAllAuthors() ([]byte, error)
	CreateBook([]byte) error
	GetAllBook() ([]byte, error)
	IssueBookID(bookStrID string, UserStrID string) error
}
