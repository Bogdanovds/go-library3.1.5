package services

import (
	"encoding/json"
	"example/m/v2/models"
	"example/m/v2/repositories"
	"fmt"
	"strconv"
)

type LibraryServ struct {
	AuthorRepository repositories.AuthorRepository
	BookRepository   repositories.BookRepository
	UserRepository   repositories.UserRepository
}

var (
	errAuthorIDNs = fmt.Errorf("author id not specified")
)

func (l *LibraryServ) GetUsersWithBooks() ([]byte, error) {
	usersID, err := l.BookRepository.ReadBorrowed()

	if err != nil {
		return nil, err
	}
	users := make([]models.User, 0, len(usersID))
	for _, id := range usersID {
		user, err := l.UserRepository.Read(id)
		if err != nil {
			continue
		}

		books, err := l.BookRepository.ReadForIssuedID(id)
		if err != nil {
			continue
		}

		user.RentedBooks = books
		users = append(users, user)
	}

	usersJson, err := json.Marshal(users)
	if err != nil {
		return nil, err
	}
	return usersJson, nil
}

func (l *LibraryServ) CreateAuthor(bytes []byte) error {
	var author models.Author
	err := json.Unmarshal(bytes, &author)
	if err != nil {
		return err
	}

	return l.AuthorRepository.Create(author)
}

func (l *LibraryServ) GetAllAuthors() ([]byte, error) {
	authors, err := l.AuthorRepository.ReadAll()
	if err != nil {
		return nil, err
	}

	for i := range authors {
		books, err := l.BookRepository.ReadForAuthorID(authors[i].Id)
		if err != nil {
			continue
		}

		authors[i].Books = books
	}

	return json.Marshal(authors)
}



func (l *LibraryServ) GetAllBook() ([]byte, error) {
	books, err := l.BookRepository.ReadAll()
	if err != nil {
		return nil, err
	}

	for i := range books {
		userID, err := l.BookRepository.ReadBorrowedID(books[i].Id)
		if err == nil {
			user, err := l.UserRepository.Read(userID)
			if err == nil {
				books[i].IssuedTo = &user
			}
		}
		authorID, err := l.BookRepository.ReadAuthorID(books[i].Id)
		if err == nil {
			author, err := l.AuthorRepository.Read(authorID)
			if err == nil {
				books[i].Author = &author
			}
		}
	}

	booksJSON, err := json.Marshal(books)
	if err != nil {
		return nil, err
	}

	return booksJSON, nil
}

func (l *LibraryServ) CreateBook(bytes []byte) error {
	var book *models.Book
	err := json.Unmarshal(bytes, book)
	if err != nil {
		return err
	}

	if book.Author.Id == 0 {
		return errAuthorIDNs
	}

	err = l.BookRepository.Create(*book)
	if err != nil {
		return err
	}

	return nil
}

func (l *LibraryServ) IssueBookID(bookStrID string, UserStrID string) error {
	bookID, err := strconv.ParseInt(bookStrID, 0, 64)
	if err != nil {
		return err
	}

	UserID, err := strconv.ParseInt(UserStrID, 0, 64)
	if err != nil {
		return err
	}

	book, err := l.BookRepository.Read(bookID)
	if err != nil {
		return err
	}

	user, err := l.UserRepository.Read(UserID)
	if err != nil {
		return err
	}

	book.IssuedTo = &user

	err = l.BookRepository.Update(book)
	if err != nil {
		return err
	}

	return nil
}
