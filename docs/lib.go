package docs

import "example/m/v2/models"

// swagger:route GET /lib/BookUserList user usersWithIssuedBooks
// Displays a list of users with checked out books
//
// responses:
//  200: description: usersWithIssuedBooksResponses
//  404: description: Not found
//  500: description: Internal server error

// swagger:response usersWithIssuedBooksResponses
//
//nolint:all
type usersWithIssuedBooksResponses struct {
	// in:body
	Body []models.User
}

// swagger:route POST /lib/AddAuthor author addAuthor
// Add new Author
//
// responses:
//  200: description:
//  500: description: Internal server error

// swagger:parameters addAuthor
//
//nolint:all
type addAuthor struct {
	// Author
	// required: true
	// in:body
	Body models.Author
}

// swagger:route GET /lib/AuthorList author authors
// Displays a list of authors
//
// responses:
//  200: description: authorsResponses
//  404: description: Not found
//  500: description: Internal server error

// swagger:response authorsResponses
//
//nolint:all
type authorsResponses struct {
	// in:body
	Body []models.Author
}

// swagger:route GET /lib/BooksList book books
// Displays a list of books
//
// responses:
//  200: description: booksResponses
//  404: description: Not found
//  500: description: Internal server error

// swagger:response booksResponses
//
//nolint:all
type booksResponses struct {
	// in:body
	Body []models.Book
}

// swagger:route POST /lib/AddAuthor book lendABook
// Issue a book by ID to a user by his ID
//
// responses:
//  200: description:
//  500: description: Internal server error

// swagger:parameters lendABook
//
//nolint:all
type bookID struct {
	// ID of book to issue
	// required: true
	// in:query
	Status int `json:"book"`
}

// swagger:parameters lendABook
//
//nolint:all
type userID struct {
	// ID of user recipient
	// required: true
	// in:query
	Status int `json:"user"`
}
