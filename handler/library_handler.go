package handler

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"io"
	"net/http"
)

func (h *Handler) libRout(r chi.Router) {

	r.Get("/BookUserList", func(w http.ResponseWriter, r *http.Request) {
		books, err := h.LibraryServices.GetUsersWithBooks()
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}
		_, err = w.Write(books)
		if WriteErr(err, w, 0) {
			return
		}
	})

	r.Post("/AddAuthor", func(w http.ResponseWriter, r *http.Request) {
		author, err := io.ReadAll(r.Body)
		if WriteErr(err, w, 0) {
			return
		}
		defer r.Body.Close()

		err = h.LibraryServices.CreateAuthor(author)
		if WriteErr(err, w, 0) {
			return
		}
	})

	r.Get("/AuthorList", func(w http.ResponseWriter, r *http.Request) {
		authors, err := h.LibraryServices.GetAllAuthors()
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}
		_, err = w.Write(authors)
		if WriteErr(err, w, 0) {
			return
		}
	})

	r.Get("/BooksList", func(w http.ResponseWriter, r *http.Request) {
		books, err := h.LibraryServices.GetAllBook()
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}
		_, err = w.Write(books)
		if WriteErr(err, w, 0) {
			return
		}
	})

	r.Post("/GiveOutBook", func(w http.ResponseWriter, r *http.Request) {
		bookId := r.URL.Query().Get("book")
		userId := r.URL.Query().Get("user")

		err := h.LibraryServices.IssueBookID(bookId, userId)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		marshal, err := json.Marshal("ok")
		if err != nil {
			return
		}
		_, _ = w.Write(marshal)
	})

}
