package handler

import (
	"encoding/json"
	"example/m/v2/docs"
	"example/m/v2/services"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type Handler struct {
	LibraryServices services.LibraryServices
}

func WriteErr(err error, w http.ResponseWriter, statusCode int) bool {
	if err != nil {
		if statusCode == 0 {
			statusCode = http.StatusInternalServerError
		}
		w.WriteHeader(statusCode)
		marshal, marshalErr := json.Marshal(err)
		if marshalErr != nil {
			_, _ = w.Write([]byte(err.Error()))
		}
		_, _ = w.Write(marshal)
	}
	return err != nil
}
func (h *Handler) Routing() *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Route("/lib", h.libRout)
	r.Get("/swagger", docs.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("static"))).ServeHTTP(w, r)
	})

	return r
}
