package main

import (
	"bufio"
	"context"
	"example/m/v2/handler"
	"example/m/v2/models"
	"example/m/v2/repositories"
	"example/m/v2/services"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"github.com/go-chi/chi/v5"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gopkg.in/yaml.v2"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"time"
)

type Config struct {
	DB struct {
		Host     string `yaml:"host"`
		Port     int    `yaml:"port"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
		DBName   string `yaml:"dbname"`
	} `yaml:"database"`
}

var (
	config           = getConfig()
	userRepository   repositories.UserRepository
	authorRepository repositories.AuthorRepository
	bookRepository   repositories.BookRepository
	libraryServices  services.LibraryServices
	Handler          *chi.Mux
)

func main() {
	getConfig()
	initHandler()

	srv := &http.Server{Addr: ":8080", Handler: Handler}

	go func() {
		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			panic(err)
		}

	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancelCnt := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelCnt()
	err := srv.Shutdown(ctx)
	if err != nil {
		panic(err)
	}

}

func getConfig() Config {
	file, err := os.ReadFile("config.yaml")
	if err != nil {
		panic(err)
	}

	var config Config
	err = yaml.Unmarshal(file, &config)
	if err != nil {
		panic(err)
	}

	return config

}

func initBD(config Config) *sqlx.DB {
	if len(config.DB.Host) <= 0 {
		config.DB.Host = "localhost"
	}
	if config.DB.Port <= 0 {
		config.DB.Port = 5432
	}
	if len(config.DB.User) <= 0 {
		config.DB.User = "postgres"
	}
	if len(config.DB.DBName) <= 0 {
		config.DB.DBName = "postgres"
	}

	db := sqlx.MustConnect("postgres",
		fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
			config.DB.Host, config.DB.Port, config.DB.User, config.DB.Password, config.DB.DBName))

	file, err := os.Open("libDB.sql")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	query := ""
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			continue
		}
		query += line + "\n"
		if line[len(line)-1] == ';' {
			_, err = db.Exec(query)
			if err != nil {
				panic(err)
			}
			query = ""
		}
	}

	if len(query) > 0 {
		_, err = db.Exec(query)
		if err != nil {
			panic(err)
		}
	}

	return db
}

func initRepository() {
	db := initBD(config)

	authorRepository = &repositories.PGAuthorRepo{DB: db}
	authors, _ := authorRepository.ReadAll()
	if authors == nil || len(authors) <= 0 {
		for i := 0; i < 10; i++ {
			err := authorRepository.Create(models.Author{
				Name: gofakeit.BookAuthor(),
			})
			if err != nil {
				continue
			}
		}
	}

	userRepository = &repositories.PGUserRepo{DB: db}
	user, _ := userRepository.ReadAll()
	if user == nil || len(user) <= 0 {
		for i := 0; i < 50; i++ {
			err := userRepository.Create(models.User{
				Name: gofakeit.Username(),
			})
			if err != nil {
				continue
			}
		}
	}

	bookRepository = &repositories.PGBookRepo{DB: db}
	books, _ := bookRepository.ReadAll()
	if books == nil || len(books) <= 0 {
		authors, err := authorRepository.ReadAll()
		if err != nil {
			panic(err)
		}
		user, err = userRepository.ReadAll()
		if err != nil {
			panic(err)
		}

		for i := 0; i < 100; i++ {
			au := &authors[rand.Intn(len(authors))]
			su := func() *models.User {
				if rand.Intn(5) == 1 {
					return &user[rand.Intn(len(user))]
				}
				return nil
			}()
			err = bookRepository.Create(models.Book{
				Author:   au,
				IssuedTo: su,
				Name:     gofakeit.BookTitle(),
			})
			if err != nil {
				continue
			}
		}
	}

}

func initServices() {
	initRepository()

	libraryServices = &services.LibraryServ{BookRepository: bookRepository, AuthorRepository: authorRepository, UserRepository: userRepository}

}

func initHandler() {
	initServices()

	h := handler.Handler{LibraryServices: libraryServices}
	Handler = h.Routing()
}
