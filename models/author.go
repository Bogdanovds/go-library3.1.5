package models

type Author struct {
	Id    int64  `json:"id" db:"id"`
	Name  string `json:"name" db:"name"`
	Books []Book `json:"books,omitempty"`
}
