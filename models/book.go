package models

type Book struct {
	Id       int64   `json:"id" db:"id"`
	Name     string  `json:"name" db:"name"`
	IssuedTo *User   `json:"issuedTo,omitempty"`
	Author   *Author `json:"author,omitempty"`
}
