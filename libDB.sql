CREATE SCHEMA IF NOT EXISTS library;

CREATE TABLE IF NOT EXISTS library.author(
    id BIGSERIAL PRIMARY KEY,
    name TEXT NOT NULL

);

CREATE TABLE IF NOT EXISTS library.user(
    id BIGSERIAL PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS library.book(
    id BIGSERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    issued_to BIGINT REFERENCES library.user(id),
    author BIGINT REFERENCES library.author(id) NOT NULL

);