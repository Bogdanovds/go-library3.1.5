module example/m/v2

go 1.19

require (
	github.com/brianvoe/gofakeit/v6 v6.22.0
	github.com/go-chi/chi/v5 v5.0.8
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.2.0
	gopkg.in/yaml.v2 v2.4.0
)
