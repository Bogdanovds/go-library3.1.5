package repositories

import "example/m/v2/models"

type UserRepository interface {
	Create(user models.User) error
	Read(id int64) (models.User, error)
	ReadAll() ([]models.User, error)
	Update(user models.User) error
	Delete(id int64) error
}

type BookRepository interface {
	Create(book models.Book) error
	Read(id int64) (models.Book, error)
	ReadForIssuedID(id int64) ([]models.Book, error)
	ReadForAuthorID(id int64) ([]models.Book, error)
	ReadAll() ([]models.Book, error)
	ReadBorrowed() ([]int64, error)
	ReadAuthorID(id int64) (int64, error)
	ReadBorrowedID(id int64) (int64, error)
	Update(book models.Book) error
	Delete(id int64) error
}

type AuthorRepository interface {
	Create(author models.Author) error
	Read(id int64) (models.Author, error)
	ReadAll() ([]models.Author, error)
	Update(author models.Author) error
	Delete(id int64) error
}
