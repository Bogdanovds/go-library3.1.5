package repositories

import (
	"example/m/v2/models"

	"github.com/jmoiron/sqlx"
)

type PGAuthorRepo struct {
	DB *sqlx.DB
}

func (repo *PGAuthorRepo) Read(id int64) (models.Author, error) {
	var author models.Author
	err := repo.DB.Get(&author, "SELECT * FROM library.author WHERE id=$1;",
		id)
	if err != nil {
		return models.Author{}, err
	}

	return author, nil
}

func (repo *PGAuthorRepo) Create(author models.Author) error {
	_, err := repo.DB.Exec(
		"INSERT INTO library.author (id, name) VALUES (DEFAULT, $1);",
		author.Name)
	if err != nil {
		return err
	}
	return nil
}

func (repo *PGAuthorRepo) ReadAll() ([]models.Author, error) {
	var authors []models.Author
	err := repo.DB.Select(&authors, "SELECT * FROM library.author;")
	if err != nil {
		return nil, err
	}

	return authors, nil
}
func (repo *PGAuthorRepo) Delete(id int64) error {
	_, err := repo.DB.Exec("DELETE FROM library.author WHERE id=$1;",
		id)
	if err != nil {
		return err
	}
	return nil
}

func (repo *PGAuthorRepo) Update(author models.Author) error {
	_, err := repo.DB.Exec("UPDATE library.author SET name=$1 WHERE id=2;",
		author.Name, author.Id)
	if err != nil {
		return err
	}
	return nil
}
