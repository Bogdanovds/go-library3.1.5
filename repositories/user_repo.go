package repositories

import (
	"example/m/v2/models"
	"github.com/jmoiron/sqlx"
)

type PGUserRepo struct {
	DB *sqlx.DB
}

func (repo *PGUserRepo) Create(user models.User) error {
	_, err := repo.DB.Exec(
		"INSERT INTO library.user (id, name) VALUES (DEFAULT, $1);",
		user.Name)
	if err != nil {
		return err
	}
	return nil
}

func (repo *PGUserRepo) Read(id int64) (models.User, error) {
	var user models.User
	err := repo.DB.Get(&user, "SELECT * FROM library.user WHERE id=$1;",
		id)
	if err != nil {
		return models.User{}, err
	}

	return user, nil
}

func (repo *PGUserRepo) ReadAll() ([]models.User, error) {
	var users []models.User
	err := repo.DB.Select(&users, "SELECT * FROM library.user;")
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (repo *PGUserRepo) Update(user models.User) error {
	_, err := repo.DB.Exec("UPDATE library.user SET name=$1 WHERE id=2;",
		user.Name, user.Id)
	if err != nil {
		return err
	}
	return nil
}

func (repo *PGUserRepo) Delete(id int64) error {
	_, err := repo.DB.Exec("DELETE FROM library.user WHERE id=$1;",
		id)
	if err != nil {
		return err
	}
	return nil
}
