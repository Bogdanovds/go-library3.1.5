package repositories

import (
	"example/m/v2/models"
	"github.com/jmoiron/sqlx"
)

type PGBookRepo struct {
	DB *sqlx.DB
}

func (repo *PGBookRepo) Create(book models.Book) error {
	var err error
	if book.IssuedTo == nil {
		_, err = repo.DB.Exec(
			"INSERT INTO library.book (id, name, author) VALUES (DEFAULT, $1, $2);",
			book.Name, book.Author.Id)
	} else {
		_, err = repo.DB.Exec(
			"INSERT INTO library.book (id, name, issued_to, author) VALUES (DEFAULT, $1, $2, $3);",
			book.Name, book.IssuedTo.Id, book.Author.Id)
	}
	if err != nil {
		return err
	}
	return nil
}

func (repo *PGBookRepo) Read(id int64) (models.Book, error) {
	var book models.Book
	err := repo.DB.Get(&book, "SELECT id,name FROM library.book WHERE id=$1;",
		id)
	if err != nil {
		return models.Book{}, err
	}

	return book, nil
}

func (repo *PGBookRepo) ReadForIssuedID(id int64) ([]models.Book, error) {
	var books []models.Book
	err := repo.DB.Select(&books, "SELECT id, name FROM library.book WHERE issued_to=$1;",
		id)
	if err != nil {
		return nil, err
	}

	return books, nil
}

func (repo *PGBookRepo) ReadForAuthorID(id int64) ([]models.Book, error) {
	var books []models.Book
	err := repo.DB.Select(&books, "SELECT id, name FROM library.book WHERE author=$1;",
		id)
	if err != nil {
		return nil, err
	}

	return books, nil
}

func (repo *PGBookRepo) ReadAll() ([]models.Book, error) {
	var books []models.Book
	err := repo.DB.Select(&books, "SELECT id, name FROM library.book;")
	if err != nil {
		return nil, err
	}

	return books, nil
}

func (repo *PGBookRepo) ReadBorrowed() ([]int64, error) {
	var usersID []int64
	err := repo.DB.Select(&usersID, "SELECT issued_to FROM library.book WHERE issued_to IS NOT NULL;")
	if err != nil {
		return nil, err
	}

	return removeDuplicate(usersID), nil
}

func (repo *PGBookRepo) ReadAuthorID(id int64) (int64, error) {
	var authorID int64
	err := repo.DB.Get(&authorID, "SELECT author FROM library.book WHERE id=$1;",
		id)
	if err != nil {
		return 0, err
	}

	return authorID, nil
}

func (repo *PGBookRepo) ReadBorrowedID(id int64) (int64, error) {
	var userID int64
	err := repo.DB.Get(&userID, "SELECT issued_to FROM library.book WHERE id=$1;",
		id)
	if err != nil {
		return 0, err
	}

	return userID, nil
}

func (repo *PGBookRepo) Update(book models.Book) error {
	_, err := repo.DB.Exec("UPDATE library.book SET name=$1 WHERE id=$2;",
		book.Name, book.Id)
	if err != nil {
		return err
	}
	return nil
}

func (repo *PGBookRepo) Delete(id int64) error {
	_, err := repo.DB.Exec("DELETE FROM library.book WHERE id=$1;",
		id)
	if err != nil {
		return err
	}
	return nil
}

func removeDuplicate[T string | int | int64](sliceList []T) []T {
	allKeys := make(map[T]bool)
	list := []T{}
	for _, item := range sliceList {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}
